<?php

use Illuminate\Database\Seeder;

class IceCreamShopTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         factory(App\IceCreamShopTableSeeder::class,2000)->create();
    }
}
